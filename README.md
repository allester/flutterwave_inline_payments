## Online Payments Demo with Flutterwave Inline

A simple illustration of how to use Flutterwave Inline to quickly embed Flutterwave in your website and accept payments


### Steps

Open you terminal and navigate to your desired location

`git clone` this repository to your desired location

Run `npm install` to install all dependencies related to this project

Open the index.ejs file located in the views folder and enter your Flutterwave public key. This can be found in your Flutterwave dashboard

Change "payment options" to your preferred option

Change "customer data" to your preferred details

More information:

[Flutterwave Inline](https://developer.flutterwave.com/docs/flutterwave-inline)

Run `npm run dev` to start your development server and navigate to the url 'http://localhost:5000'

The application should be running as expected