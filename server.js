const express = require('express')
const formidable = require('formidable')
const dotenv = require('dotenv')
const request = require('request')

dotenv.config()

const app = express()

app.set('view engine', 'ejs')
app.use(express.static('public'))

const port = process.env.PORT || 5000

app.get('/', (req, res) => {

    res.render('index')

})

app.get('/payment-made', (req, res) => {
    res.send('Payment completed')
})

// Transaction verification
let options = {
    'method': 'GET',
    'url': 'https://api.flutterwave.com/v3/transactions/876564/verify',
    'headers': {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer {{SEC_KEY}}'
    }
}

request(options, function(error, response) {
    if (error) {
        throw new Error(error)
    }
    console.log(response.body)
})


app.listen(port, () => {
    console.log(`Server listening on port ${port}`)
})